import merge from 'webpack-merge';
import { EnvironmentPlugin } from 'webpack';
import TerserPlugin from 'terser-webpack-plugin';

import common from './webpack.config';

export default merge(common, {
  mode: 'production',
  devtool: false,
  plugins: [
    new EnvironmentPlugin({
      DEBUG: 'false',
      API: 'production',
    }),
  ],
  optimization: {
    minimize: true,
    removeEmptyChunks: true,
    removeAvailableModules: true,
    occurrenceOrder: true,
    providedExports: true,
    nodeEnv: 'production',
    concatenateModules: true,
    namedChunks: true,
    namedModules: true,
    usedExports: true,
    mergeDuplicateChunks: true,
    splitChunks: {
      name: 'vendor',
      chunks: (chunk) => {
        return chunk.name !== 'main';
      },
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
    minimizer: [
      new TerserPlugin({
        parallel: 4,
        sourceMap: false,
        extractComments: false,
        terserOptions: {
          compress: true,
          ecma: 2019,
          safari10: false,
          ie8: false,
          output: {
            comments: false,
            source_map: {
              includeSources: false,
            },
          },
        },
      }),
    ],
  },
});
