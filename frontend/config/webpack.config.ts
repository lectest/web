import path from 'path';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import webpack, { EnvironmentPlugin } from 'webpack';
import GitRevisionPlugin from 'git-revision-webpack-plugin';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const gitRevision = new GitRevisionPlugin();
const config: webpack.Configuration = {
  mode: 'development',
  context: path.resolve(__dirname, '..'),
  devtool: 'cheap-eval-source-map',
  entry: './src/index.tsx',
  stats: 'errors-warnings',
  devServer: {
    writeToDisk: false,
    index: 'index.html',
    port: 3000,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader',
        exclude: [/node_modules/, /.git/],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true,
            },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: '[local]-[hash:base64:4]',
              },
            },
          },
          'fast-sass-loader',
        ],
        exclude: [/.git/],
      },
      {
        test: /\.ts(x)?$/,
        use: ['ts-loader'],
        exclude: [/node_modules/, /.git/],
      },
      {
        test: /\.(png|svg|jpg|gif|woff(2)?|ttf|eot|otf|mp3|wav)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              //   limit: 16384,
            },
          },
        ],
        exclude: [/.git/],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './assets/html/index.html',
      chunks: ['main'],
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      esModule: true,
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    }),
    new EnvironmentPlugin({
      DEBUG: 'true',
      API: 'development',
      COMMITHASH: gitRevision.commithash(),
      BRANCH: gitRevision.branch(),
    }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.scss'],
    plugins: [
      new TsconfigPathsPlugin({
        configFile: './tsconfig.json',
      }),
    ],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '..', 'dist'),
    pathinfo: false,
  },
  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
  },
};
export default config;
