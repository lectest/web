﻿#!/bin/bash
sudo rm -rf ./publish
curl --location --output back.zip --header "PRIVATE-TOKEN: {$PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/19050931/jobs/artifacts/master/download?job=build-back"
curl --location --output front.zip --header "PRIVATE-TOKEN: {$PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/19050931/jobs/artifacts/master/download?job=build-front"

unzip back.zip -d ./
unzip front.zip -d ./publish/
sudo rm back.zip
sudo rm front.zip
sleep 5s
sudo systemctl restart gd.service
